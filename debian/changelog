node-pseudorandombytes (2.0.0-2+apertis0) apertis; urgency=medium

  * Sync from debian/bookworm.

 -- Apertis CI <devel@lists.apertis.org>  Fri, 07 Apr 2023 22:14:31 +0000

node-pseudorandombytes (2.0.0-2) unstable; urgency=medium

  * Team upload

  [ Debian Janitor ]
  * Trim trailing whitespace.
  * Use secure copyright file specification URI.
  * Bump debhelper from old 9 to 12.
  * Set debhelper-compat version in Build-Depends.
  * Set upstream metadata fields: Bug-Database, Bug-Submit, Repository,
    Repository-Browse.
  * Update Vcs-* headers from URL redirect.
  * Use canonical URL in Vcs-Git.
  * Remove constraints unnecessary since stretch:
    + node-pseudorandombytes: Drop versioned constraint on node-randombytes in
      Depends.

  [ Yadd ]
  * Bump debhelper from old 12 to 13.
  * Declare compliance with policy 4.6.0
  * Add "Rules-Requires-Root: no"
  * Change section to javascript
  * Add debian/gbp.conf
  * Modernize debian/watch
  * Fix GitHub tags regex
  * Use dh-sequence-nodejs auto install
  * Drop dependency to nodejs
  * Enable upstream test
  * Add patch to replace Buffer() by Buffer.alloc()

 -- Yadd <yadd@debian.org>  Mon, 11 Oct 2021 22:34:23 +0200

node-pseudorandombytes (2.0.0-1.1apertis0) apertis; urgency=medium

  * Sync from Debian debian/bullseye.

 -- Apertis CI <devel@lists.apertis.org>  Thu, 11 Mar 2021 18:14:48 +0000

node-pseudorandombytes (2.0.0-1co1) apertis; urgency=medium

  [ Ritesh Raj Sarraf ]
  * debian/apertis/component: Set to development

 -- Emanuele Aina <emanuele.aina@collabora.com>  Sun, 21 Feb 2021 11:08:36 +0000

node-pseudorandombytes (2.0.0-1) unstable; urgency=low

  * Initial release (Closes: #864931)

 -- Pirate Praveen <praveen@debian.org>  Sat, 17 Jun 2017 21:05:02 +0530
